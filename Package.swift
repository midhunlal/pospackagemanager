// swift-tools-version:5.2
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "PosPackageManager",
    platforms: [.iOS(.v9)],
    products: [
        .library(
            name: "PosPackageManager",
            targets: ["PosPackageManager"]),
    ],
    dependencies: [
        .package(url: "https://github.com/AFNetworking/AFNetworking.git", .upToNextMajor(from: "4.0.0")),
        .package(url: "https://github.com/AliSoftware/OHHTTPStubs.git", .upToNextMajor(from: "9.0.0")),
        .package(url: "https://github.com/kishikawakatsumi/UICKeyChainStore.git", .upToNextMajor(from: "2.2.0")),
        .package(url: "https://github.com/lmirosevic/GBDeviceInfo.git", .branch("master"))
    ],
    targets: [
        .target(
            name: "PosPackageManager",
            dependencies: ["AFNetworking", "OHHTTPStubs", "UICKeyChainStore", "GBDeviceInfo"],
            publicHeadersPath: ""),
    ]
)
